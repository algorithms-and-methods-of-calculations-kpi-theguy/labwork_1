package com.kpi.yuriy.labwork1.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.kpi.yuriy.labwork1.CyclicAlgo;
import com.kpi.yuriy.labwork1.R;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import static android.app.Activity.RESULT_OK;


public class Cyclic extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final int PICKFILE_RESULT_CODE = 0;

    private String mParam1;
    private String mParam2;


    private Button button_file3;
    private Button button_own3;

    private RelativeLayout relative_cyc;
    private LinearLayout search_layout3;

    private Button button_search3;
    private EditText search_file3;
    private Button button_ok3;

    private Button button_calc3;

    private EditText editText_N;
    private EditText editText_F;

    private OnFragmentInteractionListener mListener;

    public Cyclic() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cyclic, container, false);
        button_file3 = (Button) view.findViewById(R.id.button_file3);
        button_own3 = (Button) view.findViewById(R.id.button_own3);
        relative_cyc = (RelativeLayout) view.findViewById(R.id.relative_cyc);
        search_layout3 = (LinearLayout) view.findViewById(R.id.search_layout3);
        button_search3 = (Button) view.findViewById(R.id.button_search3);
        search_file3 = (EditText) view.findViewById(R.id.search_file3);
        button_ok3 = (Button) view.findViewById(R.id.button_ok3);
        editText_N = (EditText) view.findViewById(R.id.editText_N);
        editText_F = (EditText) view.findViewById(R.id.editText_F);
        button_calc3 = (Button) view.findViewById(R.id.button_calc3);

        button_file3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                button_file3.setEnabled(false);
                button_own3.setEnabled(false);
                search_layout3.setVisibility(View.VISIBLE);
            }
        });
        button_own3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                button_file3.setEnabled(false);
                button_own3.setEnabled(false);
                relative_cyc.setVisibility(View.VISIBLE);
            }
        });
        button_search3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("text/plain");
                startActivityForResult(intent, PICKFILE_RESULT_CODE);
            }
        });
        button_ok3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                search_layout3.setVisibility(View.GONE);
                relative_cyc.setVisibility(View.VISIBLE);
                Uri buffUri = Uri.parse(search_file3.getText().toString());
                String str = "";
                String buffStr = "";
                try {
                    InputStream is = getActivity().getContentResolver().openInputStream(buffUri);
                    BufferedReader br = new BufferedReader(new InputStreamReader(is));
                    while ((str = br.readLine()) != null)
                        buffStr = str;
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                editText_N.setText(buffStr.replace("\uFEFF", ""));
            }
        });
        button_calc3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int tempN = Integer.parseInt(editText_N.getText().toString());
                CyclicAlgo cycl = new CyclicAlgo(tempN);
                double cyclCalc = cycl.calculateCyclic();
                mListener.onFragmentCyclic(cyclCalc);
                editText_F.setText(Double.toString(cyclCalc));
            }
        });
        return view;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == PICKFILE_RESULT_CODE) {
                Uri uri = data.getData();
                search_file3.setText(uri.toString());
            }
        }
        else{
            Toast.makeText(getActivity(), "Use a correct file!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentCyclic(double d);
    }
}
