package com.kpi.yuriy.labwork1.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.kpi.yuriy.labwork1.R;
import com.kpi.yuriy.labwork1.RamifiedAlgo;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import static android.app.Activity.RESULT_OK;



public class Ramified extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final int PICKFILE_RESULT_CODE = 0;

    private String mParam1;
    private String mParam2;

    private Button button_file2;
    private Button button_own2;

    private RelativeLayout relative_ram;
    private LinearLayout search_layout2;

    private Button button_calc2;

    private Button button_search2;
    private EditText search_file2;
    private Button button_ok2;

    private EditText editText_A;
    private EditText editText_B2;
    private EditText editText_X;
    private EditText editText_Y;

    private OnFragmentInteractionListener mListener;

    public Ramified() {
        // Required empty public constructor
    }

    public static Ramified newInstance(String param1, String param2) {
        Ramified fragment = new Ramified();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_ramified, container, false);
        button_file2 = (Button) view.findViewById(R.id.button_file2);
        button_own2 = (Button) view.findViewById(R.id.button_own2);
        relative_ram = (RelativeLayout) view.findViewById(R.id.relative_ram);
        search_layout2 = (LinearLayout) view.findViewById(R.id.search_layout2);
        button_search2 = (Button) view.findViewById(R.id.button_search2);
        search_file2 = (EditText) view.findViewById(R.id.search_file2);
        button_ok2 = (Button) view.findViewById(R.id.button_ok2);
        button_calc2 = (Button) view.findViewById(R.id.button_calc2);
        editText_A = (EditText) view.findViewById(R.id.editText_A);
        editText_B2 = (EditText) view.findViewById(R.id.editText_B2);
        editText_X = (EditText) view.findViewById(R.id.editText_X);
        editText_Y = (EditText) view.findViewById(R.id.editText_Y);

        button_file2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                button_file2.setEnabled(false);
                button_own2.setEnabled(false);
                search_layout2.setVisibility(View.VISIBLE);
            }
        });
        button_own2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                button_file2.setEnabled(false);
                button_own2.setEnabled(false);
                relative_ram.setVisibility(View.VISIBLE);
            }
        });
        button_search2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("text/plain");
                startActivityForResult(intent, PICKFILE_RESULT_CODE);
            }
        });
        button_ok2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                search_layout2.setVisibility(View.GONE);
                relative_ram.setVisibility(View.VISIBLE);
                Uri buffUri = Uri.parse(search_file2.getText().toString());
                String str = "";
                String[] buff = new String[3];
                ArrayList<String> strArr = new ArrayList<>();
                try {
                    InputStream is = getActivity().getContentResolver().openInputStream(buffUri);
                    BufferedReader br = new BufferedReader(new InputStreamReader(is));
                    while ((str = br.readLine()) != null)
                        strArr.add(str);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                editText_A.setText(strArr.get(0).replace("\uFEFF", ""));
                editText_B2.setText(strArr.get(1));
                editText_X.setText(strArr.get(2));
            }
        });
        button_calc2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double tempA = Double.parseDouble(editText_A.getText().toString());
                double tempB2 = Double.parseDouble(editText_B2.getText().toString());
                double tempX = Double.parseDouble(editText_X.getText().toString());
                RamifiedAlgo ram = new RamifiedAlgo(tempA, tempB2, tempX);
                double ramCalc = ram.calculateRamified();
                mListener.onFragmentRamified(ramCalc);
                editText_Y.setText(Double.toString(ramCalc));
            }
        });
        return view;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == PICKFILE_RESULT_CODE) {
                Uri uri = data.getData();
                search_file2.setText(uri.toString());
            }
        }
        else{
            Toast.makeText(getActivity(), "Use a correct file!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentRamified(double d);
    }
}
