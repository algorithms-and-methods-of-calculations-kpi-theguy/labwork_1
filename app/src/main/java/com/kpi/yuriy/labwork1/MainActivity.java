package com.kpi.yuriy.labwork1;

import android.Manifest;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.telephony.SmsManager;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.kpi.yuriy.labwork1.fragments.*;

import static android.telephony.SmsManager.RESULT_ERROR_GENERIC_FAILURE;
import static android.telephony.SmsManager.RESULT_ERROR_NO_SERVICE;
import static android.telephony.SmsManager.RESULT_ERROR_NULL_PDU;
import static android.telephony.SmsManager.RESULT_ERROR_RADIO_OFF;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, Linear.OnFragmentInteractionListener,
        Ramified.OnFragmentInteractionListener, Cyclic.OnFragmentInteractionListener{

    private Main fMain;
    private Linear fLin;
    private Ramified fRam;
    private Cyclic fCycl;

    private double y1_lin = 0;
    private double y_ram = 0;
    private double f_cycl = 0;

    private BroadcastReceiver sbr;
    private BroadcastReceiver dbr;

    private static final int REQUEST_SENT_SMS = 2;
    private static final int REQUEST_EXTERNAL_STORAGE = 0;
    private static final String[] PERMISSIONS = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.SEND_SMS
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                phoneDialog();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        FragmentTransaction fragT = getFragmentManager().beginTransaction();

        fMain = new Main();
        fLin = new Linear();
        fRam = new Ramified();
        fCycl = new Cyclic();
        fragT.add(R.id.container, fMain);
        fragT.commit();

        verifyPermission(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_about) {
            Snackbar.make(findViewById(R.id.container), "Author Yuriy Butskiy. KPI 2017, All rights reserved",
                    Snackbar.LENGTH_LONG).setAction("Action", null).show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        FragmentTransaction fragT = getFragmentManager().beginTransaction();

        if (id == R.id.nav_main) {
            fragT.replace(R.id.container, fMain);
        } else if (id == R.id.nav_linear) {
            fragT.replace(R.id.container, fLin);
        } else if (id == R.id.nav_ramified) {
            fragT.replace(R.id.container, fRam);
        } else if (id == R.id.nav_cyclic) {
            fragT.replace(R.id.container, fCycl);
        }
        fragT.commit();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void verifyPermission (Activity activity){
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.READ_EXTERNAL_STORAGE);
        int permision2 = ActivityCompat.checkSelfPermission(activity, Manifest.permission.SEND_SMS);

        if (permission != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(activity, PERMISSIONS, REQUEST_EXTERNAL_STORAGE);
        }

        if (permision2 != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(activity, PERMISSIONS, REQUEST_SENT_SMS);
        }
    }

    public void phoneDialog(){
        AlertDialog.Builder dialogBuild = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View editView = inflater.inflate(R.layout.dialog_with_edit, null);
        dialogBuild.setView(editView);

        dialogBuild.setTitle("Send SMS with result.");
        dialogBuild.setMessage("Enter the phone-number to sent result:");
        final String message = "Result:\ny1="+y1_lin+"\ny="+y_ram+"\nf="+f_cycl;
        dialogBuild.setMessage("Result will be in that type:\n"+message);
        dialogBuild.setPositiveButton("Send", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                EditText phoneText = (EditText) editView.findViewById(R.id.phoneText);
                String phoneNumber = phoneText.getText().toString();
                sendSMS(phoneNumber, message);
            }
        });
        dialogBuild.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        AlertDialog a = dialogBuild.create();
        a.show();
    }

    public void sendSMS(String destination, String text){
        final String SEND = "SMS_SEND";
        final String DELIVERED = "SMS_DELIVERED";

        PendingIntent sendingPI = PendingIntent.getBroadcast(this, 0, new Intent(SEND), 0);
        PendingIntent deliveredPI = PendingIntent.getBroadcast(this, 0, new Intent(DELIVERED), 0);

        sbr = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                switch (getResultCode()){
                    case RESULT_OK:
                        Toast.makeText(getBaseContext(), "SMS send", Toast.LENGTH_SHORT).show();
                        break;
                    case RESULT_ERROR_GENERIC_FAILURE:
                        Toast.makeText(getBaseContext(), "SMS error. Generic Failure", Toast.LENGTH_SHORT).show();
                        break;
                    case RESULT_ERROR_NO_SERVICE:
                        Toast.makeText(getBaseContext(), "SMS error. No Service", Toast.LENGTH_SHORT).show();
                        break;
                    case RESULT_ERROR_NULL_PDU:
                        Toast.makeText(getBaseContext(), "SMS error. Null PDU", Toast.LENGTH_SHORT).show();
                        break;
                    case RESULT_ERROR_RADIO_OFF:
                        Toast.makeText(getBaseContext(), "SMS error. Radio Off", Toast.LENGTH_SHORT).show();
                        break;
                }
                unregisterReceiver(sbr);
            }
        };
        registerReceiver(sbr, new IntentFilter(SEND));

        dbr = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                switch (getResultCode()) {
                    case RESULT_OK:
                        Toast.makeText(getBaseContext(), "SMS delivered", Toast.LENGTH_SHORT).show();
                        break;
                    case RESULT_CANCELED:
                        Toast.makeText(getBaseContext(), "SMS not delivered", Toast.LENGTH_SHORT).show();
                        break;
                }
                unregisterReceiver(dbr);
            }
        };
        registerReceiver(dbr, new IntentFilter(DELIVERED));

        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(destination, null, text, sendingPI, deliveredPI);

    }

    @Override
    public void onFragmentCyclic(double d) {
        f_cycl = d;
    }

    @Override
    public void onFragmentLinear(double d) {
        y1_lin = d;
    }

    @Override
    public void onFragmentRamified(double d) {
        y_ram = d;
    }
}
