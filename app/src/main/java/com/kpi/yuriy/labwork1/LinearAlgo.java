package com.kpi.yuriy.labwork1;


public class LinearAlgo {
    private double b;
    private double c;

    public LinearAlgo(double b, double c) {
        this.b = b;
        this.c = c;
    }

    public double calc (double p, double q){
        double k = (p*Math.sqrt(q))/(Math.pow(2, p));
        return k;
    }

    public double calculateLinear (){
        double k1 = calc(b, c);
        double k2 = calc(c, b);
        double res = k1 - k2;
        return res;
    }
}
