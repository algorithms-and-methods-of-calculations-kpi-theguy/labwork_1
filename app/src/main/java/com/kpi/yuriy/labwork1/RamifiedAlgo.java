package com.kpi.yuriy.labwork1;


public class RamifiedAlgo {
    private double a, b ,x;

    public RamifiedAlgo(double a, double b, double x) {
        this.a = a;
        this.b = b;
        this.x = x;
    }

    public double calculateRamified (){
        double y;
        if (x > 0){
            y = (a*Math.pow(x, 3))-(b*Math.pow(x, 2));
        }
        else{
            y = (b*Math.pow(x, 3))+(a*Math.pow(x, 2));
        }
        return y;
    }
}
