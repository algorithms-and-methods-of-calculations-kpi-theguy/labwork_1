package com.kpi.yuriy.labwork1;

public class CyclicAlgo {
    private int n;

    public CyclicAlgo(int n) {
        this.n = n;
    }

    public int fact (int f){
        int res;
        if (f == 0){
            return 1;
        }
        res = f * fact(f - 1);
        return res;
    }

    public int calculateCyclic(){
        int res1 = 1;
        int res2 = 0;
        for (int j = 0; j < n; j++){
            res1 *= fact(j);
        }
        for (int i = 0; i < n; i++){
            res2 += fact(i);
        }
        int res = res1 - res2;
        return res;
    }
}
