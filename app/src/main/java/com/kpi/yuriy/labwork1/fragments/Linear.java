package com.kpi.yuriy.labwork1.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.kpi.yuriy.labwork1.LinearAlgo;
import com.kpi.yuriy.labwork1.R;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import static android.app.Activity.RESULT_OK;


public class Linear extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final int PICKFILE_RESULT_CODE = 1;

    private String mParam1;
    private String mParam2;

    private Button button_file1;
    private Button button_own1;

    private RelativeLayout relative_linear;
    private LinearLayout search_layout1;


    private Button button_calc1;

    private Button button_search1;
    private Button button_ok1;
    private EditText search_file1;

    private EditText editText_B1;
    private EditText editText_C1;
    private EditText editText_Y1;


    private OnFragmentInteractionListener mListener;


    public Linear() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_linear, container, false);
        button_file1 = (Button) view.findViewById(R.id.button_file1);
        button_own1 = (Button) view.findViewById(R.id.button_own1);
        relative_linear = (RelativeLayout) view.findViewById(R.id.relative_linear);
        search_layout1 = (LinearLayout) view.findViewById(R.id.search_layout1);
        button_search1 = (Button) view.findViewById(R.id.button_search1);
        search_file1 = (EditText) view.findViewById(R.id.search_file1);
        button_ok1 = (Button) view.findViewById(R.id.button_ok1);
        editText_B1 = (EditText) view.findViewById(R.id.editText_B1);
        editText_C1 = (EditText) view.findViewById(R.id.editText_C1);
        editText_Y1 = (EditText) view.findViewById(R.id.editText_Y1);
        button_calc1 = (Button) view.findViewById(R.id.button_calc1);

        button_file1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                button_file1.setEnabled(false);
                button_own1.setEnabled(false);
                search_layout1.setVisibility(View.VISIBLE);
            }
        });
        button_own1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                button_file1.setEnabled(false);
                button_own1.setEnabled(false);
                relative_linear.setVisibility(View.VISIBLE);
            }
        });
        button_search1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("text/plain");
                startActivityForResult(intent, PICKFILE_RESULT_CODE);
            }
        });
        button_ok1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                search_layout1.setVisibility(View.GONE);
                relative_linear.setVisibility(View.VISIBLE);
                Uri buffUri = Uri.parse(search_file1.getText().toString());
                String str = "";
                ArrayList<String> strArr = new ArrayList<>();
                try {
                    InputStream is = getActivity().getContentResolver().openInputStream(buffUri);
                    BufferedReader br = new BufferedReader(new InputStreamReader(is));
                    while ((str = br.readLine()) != null)
                        strArr.add(str);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                editText_B1.setText(strArr.get(0).replace("\uFEFF", ""));
                editText_C1.setText(strArr.get(1));
            }
        });
        button_calc1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double tempB1 = Double.parseDouble(editText_B1.getText().toString());
                double tempC = Double.parseDouble(editText_C1.getText().toString());
                LinearAlgo lin = new LinearAlgo(tempB1, tempC);
                double linCalc = lin.calculateLinear();
                mListener.onFragmentLinear(linCalc);
                editText_Y1.setText(Double.toString(linCalc));
            }
        });

        return view;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == PICKFILE_RESULT_CODE) {
                Uri uri = data.getData();
                search_file1.setText(uri.toString());
            }
        }else{
            Toast.makeText(getActivity(), "Use a correct file!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentLinear(double d);
    }

}
